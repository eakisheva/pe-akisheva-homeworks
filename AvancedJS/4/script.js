const API = "https://ajax.test-danit.com/api/swapi/films";
const content = document.querySelector('.content');

const sendRequest = async (url) => {
	const response = await fetch(url);
	const result = await response.json();
	return result;
}
const getMovies = (url) => {
	sendRequest(url)
		.then(data => {
            data.forEach(({episodeId, name, openingCrawl, characters}) => {
				content.insertAdjacentHTML('afterbegin', `
				<div class = 'movie-item'>
					<p class = 'episod-id'>Episod ${episodeId} ${name}</p>
                    <p class = 'description'>${openingCrawl}</p>
                    <div class = 'loader'></div>
				</div>
			`)
            getCharacters(characters)
			});
		})
		.catch((error) => {
			console.log(error.message);
		})
}

const getCharacters = (characters) => {
    const movieItem = document.querySelector('.movie-item');
    const listOfChar = document.createElement('ul');
    listOfChar.classList.add('list-of-characters');
    movieItem.append(listOfChar);
    const requests = characters.map( url => fetch( url ) );
	Promise.all( requests )
		.then( responses => {
			const results = responses.map( response => response.json() );
			Promise.all( results )
                .then( results => {
                    results.forEach(({name}) => {
                        const listItem = document.createElement('li');
                        listItem.innerText = name;
                        listOfChar.append(listItem);
                        
                    })
			    } )
                .catch((error) => {
                    console.log(error.message);
                })
                .finally (() => {
                    const loader = movieItem.querySelector('.loader');
                    loader.remove();
                })
		} );
}

getMovies(API);

