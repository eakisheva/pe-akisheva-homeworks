const APIu = "https://ajax.test-danit.com/api/json/users";
const APIp = "https://ajax.test-danit.com/api/json/posts";

const cardsContainer = document.querySelector('.cards-container');
const loader = document.querySelector('.loader');
class Card {
    constructor(name, username, email, title, body) {
      this.name = name;
      this.username = username;
      this.email = email;
      this.title = title;
      this.body = body;
    }
    deletePost() {
        const delBtn = document.querySelector('#delete');
        delBtn.addEventListener('click', () => {
            const postId = delBtn.closest(".user-card").id;
            sendRequest(`${APIp}/${postId}`, 'DELETE')
                .then(response => {
                    if (response.status === 200) {
                        delBtn.closest(".user-card").remove();
                    }
                });
        });
    }
}    
const sendRequest = async (url, method = 'GET', config) => {
	return await fetch(url, {
        method,
        ...config
    })
        .then((response) => {
            if(response.ok){
                if(method === 'GET' || method === 'POST' || method === 'PUT') {
                    return response.json()
                }
                return response            
            } else {
                return new Error('error')
            }
        })
};

const createUserCard = (url) => {
	sendRequest(url)
		.then(data => {
            data.forEach(({id, name, username, email}) => {
                const card = new Card(name, username, email);
                getUserPosts(id, card);
            });
        })
}

const getUserPosts = (requiredId, card) => {
    sendRequest(APIp)
		.then(data => {
            data.forEach(({id, userId, title, body}) => {
                if (requiredId === userId) {
                    card.title = title;
                    card.body = body;
                    card.id = userId;
                    renderCard(card, id);
                } 
            });    
        })	
};

const renderCard = (card, cardId) => {
    cardsContainer.insertAdjacentHTML( 'afterbegin', `
        <div class = 'user-card' id = '${cardId}'>
            <button id = 'delete'>DELETE</button>   
            <h3 class = 'title'>${card.title}</h3>
            <div class = 'description'>${card.body}</div>
            <footer class = 'contacts'>
                <p class = 'user-name'>${card.name}  ${card.username}</p>
                <p class = 'email'>${card.email}</p>
            </footer>
        </div>
    `)
    card.deletePost();
    loader.remove();
};

createUserCard(APIu);