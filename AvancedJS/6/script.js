const button = document.querySelector('.btn');
const API = 'https://api.ipify.org/?format=json';
const APIaddress = 'http://ip-api.com/';

const sendRequest = (url) => fetch(url).then(response => response.json())

button.addEventListener('click', async () => {
    const ip = await sendRequest (API);
    const address = await sendRequest (`${APIaddress}json/${ip.ip}`);
    render(address);
})
function render({country, regionName, city, zip, timezone}) {
    button.insertAdjacentHTML('afterend', `
        <div class="address">
            <h3>Ваша локація</h3>
            <p>Країна: ${country}</p>
            <p>Регіон: ${regionName}</p>
            <p>Місто: ${city}</p>
            <p>Індекс: ${zip}</p>
            <p>Часовий пояс: ${timezone}</p>
        </div>
    `)
}