import React, { Component } from 'react'
import "./button.scss"

export default class Button extends Component {
    
    render() {
       const {config, showModal} = this.props;
       const {text, bColor} = config;

        return(
            <button className="button" type="button" style = {{backgroundColor:`${bColor}`}} onClick={
                ()=>showModal()
           }
            > {text} </button>
        ) 
    }}