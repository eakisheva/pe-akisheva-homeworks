import React, {Component} from 'react';

import Button from './components/buttons';
import Modal from './components/modal';
import ModalBtn from './components/modalBtn';

class App extends Component {
	state = {
        show1: false,
		show2:false,
		firstButton: {
			text: 'Open first modal',
			bColor: '#f37474',
		},
		secondButton: {
			text: 'Open second modal',
			bColor: '#6ed95d',
		},
     	firstModal: {
			text : "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
			header : "Do you want to delete this file?",
			closeButton : true,
			actions:(<><ModalBtn text = "ok" closeModal = {()=>this.showModal("first")}/>
			<ModalBtn text = "cancel" closeModal = {()=>this.showModal("first")}/></>)
		},
		secondModal: {
			text : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque tempore nihil, eaque sed ipsa ducimus fugit tempora tenetur laudantium eius.",
			header : "The file was created",
			closeButton : false,
			actions:(<><ModalBtn text = "ok" closeModal = {()=>this.showModal("second")}/>
			<ModalBtn text = "cancel" closeModal = {()=>this.showModal("second")}/></>)
		},

	};
    showModal = str => {      	
		if (str === "first") {
			this.setState({show1: !this.state.show1})
		  } else {
			this.setState({show2: !this.state.show2})
		  }
      };

	render() {
		
		return (
			<>
			<div className='button-container'>
				<Button config = {this.state.firstButton} showModal = {()=>this.showModal("first")}/>
				<Button config = {this.state.secondButton} showModal = {()=>this.showModal("second")}/>
			</div>		
			{this.state.show1 && <Modal configModal = {this.state.firstModal} onClose={()=>this.showModal("first")}/>}
			{this.state.show2 &&<Modal configModal = {this.state.secondModal} onClose={()=>this.showModal("second")}/>}				
			</>
			
		)
	}
}

export default App;