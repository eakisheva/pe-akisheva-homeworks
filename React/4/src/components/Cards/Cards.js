import React from 'react';
import PropTypes from "prop-types";

import ItemCard from '../ItemCard';
import Modal from '../Modal';
import Button from '../Button';
import "./cards.scss"
import { useSelector } from 'react-redux';
import {selectCards} from '../../store/selectors'

const Cards = () => {
    const cards = useSelector(selectCards)
    // const {cards, inFavorite, addToCart, addRemoveFavorite, show, showModal} = props;
    // const modalConfig = {
    //     text: "Товар додано до кошику",
    //     actions: (<Button className='modal-button' text = "Ok" onclick = {showModal}/>)
    // }             
    return (
        <div className='cards-wrapper'>
            {cards && cards.map((card, index) => {
                return (<ItemCard 
                            cardId={card.cardId} 
                            key = {index} 
                            // showModal = {showModal}
                            // inFavorite = {inFavorite}
                            // addToCart = {addToCart}
                            // addRemoveFavorite = {addRemoveFavorite}
                        />)
            }
           ) }
           {/* {show && <Modal show = {show} showModal = {showModal} modalConfig = {modalConfig} />}*/}
        </div> 
    )
}

export default Cards