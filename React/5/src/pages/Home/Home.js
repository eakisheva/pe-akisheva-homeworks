import React, {useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { selectCards, selectShow } from '../../store/selectors';

import ItemCard from '../../components/ItemCard';
import Modal from '../../components/Modal';
import Button from '../../components/Button';

import { fetchCards } from '../../store/actions';

import './Home-cards.scss'

const Home = ({addRemoveFavorite, showModal}) => {
    const dispatch = useDispatch();
    const cards = useSelector(selectCards)
    const show = useSelector(selectShow);

    useEffect(() => {
        return () => {
            dispatch(fetchCards())
        }
    },[]);

    const modalConfig = {
        text: "Товар додано до кошику",
        actions: (<Button className='modal-button' text = "Ok" onclick = {showModal}/>)
    }    
    return (
        <div className='cards-wrapper'>
            {cards && cards.map((card, index) => {
                return (<ItemCard 
                            cardId={card.cardId} 
                            key = {index} 
                            addRemoveFavorite = {addRemoveFavorite}
                            showModal = {showModal}
                        />)
            }
           ) }
           {show && <Modal showModal = {showModal} modalConfig = {modalConfig} />}
        </div> 
    )
}
export default Home;