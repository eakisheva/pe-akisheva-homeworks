import React from 'react';
import PropTypes from "prop-types";

import ItemCard from '../ItemCard';
import Modal from '../Modal';
import Button from '../Button';
import "./cards.scss"

const Cards = (props) => {
    const {cards, inFavorite, addToCart, addRemoveFavorite, show, showModal} = props;
    const modalConfig = {
        text: "Товар додано до кошику",
        actions: (<Button className='modal-button' text = "Ok" onclick = {showModal}/>)
    }             
    return (
        <div className='cards-wrapper'>
            {cards && cards.map((card, index) => {
                return (<ItemCard 
                            card={card} 
                            key = {index} 
                            showModal = {showModal}
                            inFavorite = {inFavorite}
                            addToCart = {addToCart}
                            addRemoveFavorite = {addRemoveFavorite}
                        />)
            }
           ) }
           {show && <Modal show = {show} showModal = {showModal} modalConfig = {modalConfig} />}
        </div>
    )
}
Cards.propTypes = {
    cards: PropTypes.array,
    show: PropTypes.bool,
    showModal: PropTypes.func,
    inFavorite: PropTypes.array,
    addToCart: PropTypes.func,
    addRemoveFavorite: PropTypes.func
};
export default Cards