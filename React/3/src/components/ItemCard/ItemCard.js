import React, {useState, useEffect} from 'react';
import PropTypes from "prop-types";

import {ReactComponent as IconCart} from "./img/cart.svg"
import {ReactComponent as IconFavorite} from "./img/favoriteN.svg"
import "./item.scss"

const ItemCard = (props) => {
    
    const {card, showModal, inFavorite, addToCart, addRemoveFavorite} = props;
    const {cardId, name, price, vendorCode, url, power} = card;

    const [isFavorite, setIsFavorite] = useState(false);
    
    const changeIsFavorite = (cardId) => {
        setIsFavorite(!isFavorite);
    }
    useEffect(() => {
        if (inFavorite.includes(cardId)) {
            changeIsFavorite(cardId)
        }
    },[])
    
    return (
        <div className='card-wrapper' id = {cardId}>
            <div className='card-item'>
                <a href='./'>
                    <div className='card-item__img'>
                        <img src={url} alt={name}/>
                    </div>                        
                </a>
                <div className='card-item__vendor'>{vendorCode}</div>
                <div className='card-item__price'>{price} грн</div>
                <div className='card-item__icons'>                       
                    <IconFavorite id = {`icon-${cardId}`}                             
                    className={isFavorite? 'icon-favorite isFavorite': 'icon-favorite'}
                            onClick={() => {
                                addRemoveFavorite(cardId);
                                changeIsFavorite(cardId)
                    }}/>
                    <IconCart onClick = {() => {
                                showModal();
                                addToCart(cardId)}}/>
                </div>
                <div className='card-item__name'>{name}</div>
                <div className='card-item__power'>{power}</div>
            </div>
        </div>
        )
}
ItemCard.propTypes = {
    card: PropTypes.object,
    showModal: PropTypes.func,
    inFavorite: PropTypes.array,
    addToCart: PropTypes.func,
    addToFavorite: PropTypes.func 
}

export default ItemCard