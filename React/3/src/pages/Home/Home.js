import React from 'react';

import Cards from '../../components/Cards';

const Home = ({cards, inCart, inFavorite, show, showModal,addToCart, addRemoveFavorite}) => {
    return (
        <Cards cards = {cards} 
                inCart = {inCart}
                showModal = {showModal}
                show = {show}
                inFavorite = {inFavorite}
                addToCart = {addToCart}
                addRemoveFavorite = {addRemoveFavorite}/>
    )
}
export default Home;