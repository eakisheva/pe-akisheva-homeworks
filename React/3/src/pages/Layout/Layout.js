import { Outlet} from "react-router-dom";

import Header from "../../components/Header";
import Footer from "../../components/Footer/Footer";

import './layout.scss'

const Layout = ({inCart,inFavorite}) => {
    
    return (
        <div className="page-wrapper">
        <Header inCart = {inCart}
                inFavorite = {inFavorite}/>
        <div className="main">
            <Outlet/>
        </div>
        
        <Footer/>
        </div>
    )
}

export default Layout