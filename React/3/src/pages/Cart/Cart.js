import React from "react";
import PropTypes from "prop-types";

import ItemCard from "../../components/ItemCard";
import Modal from "../../components/Modal";
import Button from "../../components/Button";
import './Cart.scss'

const Cart = ({cards, inCart, removeCard, selectedCard, inFavorite, show, showModal}) => {
    
    const modalConfig = {
        text: "Ви дійсно хочете видалити товар?",
        actions: (<><Button className='modal-button' text = "Ok" onclick = {removeCard}/>
                    <Button className='modal-button' text = "Cancel" onclick = {showModal}/></>)
    }
    let cardsInCart = []
    inCart.forEach(id => {
    let searchingCard = cards.find(card => card.cardId === id);
    cardsInCart.push(searchingCard)});
    
    const cartGroupById = Object.values(cardsInCart.reduce((value, object) => {
        if (value[object.cardId]) {
            value[object.cardId].counter++;      
        } else {
            value[object.cardId] = { ...object , counter : 1 };
        }
        return value;
    }, {}));  


    return(
        <div className='cart-page'>
            <p className='cart-page__title'>Товари у кошику</p>
            <div className='cards-wrapper'>
                {cartGroupById.map((card, index) => {
                     return (
                        <div className="cart-page__item" key = {index}>
                            <ItemCard 
                            card={card} 
                            inCart = {inCart}
                            inFavorite = {inFavorite}
                            />
                            <div id = {`btn-${card.cardId}`}>
                                <p className="item-amount">{card.counter} шт.</p>
                                <Button text = "Видалити" 
                                        className = "remove-item" 
                                        onclick = {() =>{showModal(); selectedCard(card.cardId)}}/>
                            </div>
                        </div>    
                            )
                    }
           ) }
           {show && <Modal show = {show} showModal = {showModal} modalConfig = {modalConfig} />}
            </div>
        </div>   
    )
}

export default Cart

ItemCard.propTypes = {
    cards: PropTypes.array,
    inCart: PropTypes.array,
    inFavorite: PropTypes.array,
    removeCard: PropTypes.func
}
