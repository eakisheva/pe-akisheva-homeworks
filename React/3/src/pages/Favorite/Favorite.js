import React from 'react';
import PropTypes from "prop-types";

import ItemCard from '../../components/ItemCard';

import './Favorite.scss';

const Favorite = ({cards, inFavorite, addRemoveFavorite}) => {
    let favoriteCards = cards.filter(card => inFavorite.includes(card.cardId))
    
    return (
        <div className='favorite-page'>
            <p className='favorite-page__title'>Ваші улюблені товари</p>
            <div className='cards-wrapper'>
                {favoriteCards.map((card, index) => {
                     return (<ItemCard 
                            card={card} 
                            key = {index}                         
                            inFavorite = {inFavorite}
                            addRemoveFavorite = {addRemoveFavorite}
                            />)
                    }
           ) }
            </div>
        </div>   
    )

}
ItemCard.propTypes = {
    card: PropTypes.object,
    inFavorite: PropTypes.array,
    addToFavorite: PropTypes.func 
}
export default Favorite