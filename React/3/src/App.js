import React, {useState, useEffect} from 'react';
import {Route, Routes} from "react-router-dom";

import Layout from './pages/Layout/Layout';
import Home from './pages/Home';
import Cart from './pages/Cart';
import Favorite from './pages/Favorite';
import ErrorPage from './pages/ErrorPage';

import { sendRequest } from './helpers/sendrequest';

const App = () => {
  const localStItemsFavorite = localStorage.getItem("inFavorite") ? JSON.parse(localStorage.getItem("inFavorite")) : [];
  const localStItemsCart = localStorage.getItem("inCart") ? JSON.parse(localStorage.getItem("inCart")) : [];
  
  const [cards, setCards] = useState([]);
  const [inCart, setInCart] = useState(localStItemsCart);
  const [inFavorite, setInFavorite] = useState(localStItemsFavorite);
  const [show, setShow] = useState(false);
  const [selected, setSelected] = useState();
        
  useEffect(() => {
    sendRequest("./cardsArr.json")
      .then (({cards}) => {
        setCards(cards)
      });
  },[])

  useEffect(() => {
    localStorage.setItem(
      "inCart",
      JSON.stringify(inCart)
      )},[inCart]);
  useEffect(() => {    
    localStorage.setItem(
      "inFavorite",
      JSON.stringify(inFavorite)
      );
  },[inFavorite]);
   
  const addToCart = (cardId) => {
    setInCart(prev => [...prev, cardId]);
  } 
  
  const addRemoveFavorite = (cardId) => {
    let favoriteSetter = [...inFavorite];
    if (inFavorite.includes(cardId)){ 
      favoriteSetter = inFavorite.filter(id => id !== cardId);
    } else {
      favoriteSetter.push(cardId)
      }
    setInFavorite(favoriteSetter) 
    }

  const removeCard = () => {
    const cardId = selected;
    let newCart = [...inCart];
    const index = newCart.indexOf(cardId);
    if (index > -1) { 
        newCart.splice(index, 1);
    }
    setInCart(newCart)
  }  

  const selectedCard = (cardId) => setSelected(cardId) ; 

  const showModal = () => {
    setShow(!show)
  };  

    return(
      <Routes>
        <Route path = '/' element = {<Layout inCart = {inCart} inFavorite = {inFavorite}/>}> 
          <Route index element = {cards && (<Home
                                    cards = {cards}
                                    show = {show}
                                    showModal = {showModal} 
                                    inCart = {inCart}
                                    inFavorite = {inFavorite}
                                    addToCart = {addToCart}                                  
                                    addRemoveFavorite = {addRemoveFavorite}/>)}/>
          <Route path = '/cart' element = {<Cart 
                                    cards = {cards} 
                                    inCart = {inCart}
                                    show = {show}
                                    showModal = {showModal}
                                    removeCard = {removeCard}
                                    selectedCard = {selectedCard}
                                    inFavorite = {inFavorite} />}/>
          <Route path = '/favorite' element = {<Favorite 
                                    cards = {cards} 
                                    inFavorite = {inFavorite}                                   
                                    addRemoveFavorite = {addRemoveFavorite}/>}/>
          <Route path = '*' element = {<ErrorPage/>}/>
        </Route>  
      </Routes>
    )
}
export default App;
