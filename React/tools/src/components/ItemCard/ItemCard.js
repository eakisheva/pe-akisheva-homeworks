import React, { Component } from 'react';
import PropTypes from "prop-types";

import {ReactComponent as IconCart} from "./img/cart.svg"
import {ReactComponent as IconFavorite} from "./img/favoriteN.svg"
import "./item.scss"

export default class ItemCard extends Component {
    state = {
        isFavorite: false
    }
    changeIsFavorite = (cardId) => {
        this.setState({isFavorite: !this.state.isFavorite});
    }
    
    componentDidMount(){
        this.props.inFavorite.forEach(item => {
            if (item.cardId === this.props.card.cardId) {
                this.changeIsFavorite(this.props.card.cardId)
                }
            })
    }

     render(){
        const {card, showModal, addToCart, addRemoveFavorite} = this.props;
        const {cardId, name, price, vendorCode, url, power} = card;

        return (
            <div className='card-wrapper' id = {cardId}>
                <div className='card-item'>
                    <a href='./'>
                        <div className='card-item__img'>
                            <img src={url} alt={name}/>
                        </div>                        
                    </a>
                    <div className='card-item__vendor'>{vendorCode}</div>
                    <div className='card-item__price'>{price} грн</div>
                    <div className='card-item__icons'>                       
                        <IconFavorite id = {`icon-${cardId}`} 
                            className={this.state.isFavorite? 'icon-favorite isFavorite': 'icon-favorite'} 
                            onClick={() => {
                                addRemoveFavorite(card);
                                this.changeIsFavorite(cardId)
                        }}/>
                        <IconCart onClick = {() => {
                                    showModal();
                                    addToCart({card})}}/>
                    </div>
                    <div className='card-item__name'>{name}</div>
                    <div className='card-item__power'>{power}</div>
                </div>
            </div>
        )
    }
}
ItemCard.propTypes = {
    card: PropTypes.object,
    showModal: PropTypes.func,
    inFavorite: PropTypes.array,
    addToCart: PropTypes.func,
    addToFavorite: PropTypes.func 
}