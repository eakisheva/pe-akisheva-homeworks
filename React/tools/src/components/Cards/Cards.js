import React, { Component } from 'react';
import PropTypes from "prop-types";

import ItemCard from '../ItemCard';
import Modal from '../Modal';
import "./cards.scss"

export default class Cards extends Component {
    state = {
        show: false
    }
    
    showModal = () => {      	        
        this.setState({show: !this.state.show})
    }   
    
    render(){
        const {cards, inFavorite, addToCart, addRemoveFavorite} = this.props;
        
        return (
            <div className='cards-wrapper'>
                {cards && cards.map((card, index) => {
                    return (<ItemCard 
                                card={card} 
                                key = {index} 
                                showModal = {this.showModal}
                                inFavorite = {inFavorite}
                                addToCart = {addToCart}
                                addRemoveFavorite = {addRemoveFavorite}
                                />)
                }
               ) }
               {this.state.show && <Modal show = {this.state.show} showModal = {this.showModal}/>}
            </div>
        )
    }
}
Cards.propTypes = {
    cards: PropTypes.array,
    inFavorite: PropTypes.array,
    addToCart: PropTypes.func,
    addRemoveFavorite: PropTypes.func
}