import React, {Component} from 'react';
import PropTypes from "prop-types";

import {ReactComponent as IconLogo} from "./img/logo.svg"
import {ReactComponent as IconFavorite} from "./img/favorite.svg"
import {ReactComponent as IconCart} from "./img/cart.svg"
import './header.scss'

export default class Header extends Component {
   
    render(){
        const {inCart, inFavorite} = this.props;
        let itemsInCart;
        if (inCart.length > 0){
            itemsInCart = inCart.length
        }
        let itemsInFavorite;
        if (inFavorite.length > 0){
            itemsInFavorite = inFavorite.length
        }
        
        return(
            <header className='header'>
                <IconLogo/> 
                <div className='header__slogan'>Продукція Макіта від офіційного дилера</div>
                <div className='header__icons'>
                    <IconFavorite className='header__icons-favorite'/>
                    <div id='favorite'>{itemsInFavorite}</div>
                    <IconCart className='header__icons-cart'/>
                    <div id='cart'>{itemsInCart}</div>
                </div>                
            </header>
        )
   }
    
}
Header.propTypes = {
    inCart: PropTypes.array,
    inFavorite: PropTypes.array
}