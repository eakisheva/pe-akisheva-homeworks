import React, {Component} from 'react';

import "./footer.scss"

export default class Footer extends Component {
    render(){
        return(
            <footer className='contacts'>
                <div className='contacts-wrapper'>
                    <p>Copyright &copy; 2022 Akisheva</p>
                    <p>phone 0977777711</p>
                    <p>Working hours: 9am - 6pm</p>
                </div>
            </footer>
        )
    }
}