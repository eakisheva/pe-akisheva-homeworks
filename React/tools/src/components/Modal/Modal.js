import React, { Component } from 'react';
import PropTypes from "prop-types";

import "./modal.scss"

export default class Modal extends Component {
    render () {
        const {showModal} = this.props;
        
        return(            
            <div className='modal-wrapper'onClick = {showModal}>
                <div className='modal'>
                    <p className='content'>Товар додано до кошику</p>
                    <span className = "close-btn" onClick = {showModal}></span>
                    <button className='modal-button' type='submit'>OK</button>
                </div>
            </div>
        )
            }
}
Modal.propTypes = {
    showModal: PropTypes.func, 
}