import React, {Component} from 'react';

import Header from "./components/Header"
import Cards from './components/Cards';
import Footer from './components/Footer/Footer';

import { sendRequest } from './helpers/sendrequest';

class App extends Component {
  state = {
    cards: [],
    inCart: [],
    inFavorite: []
  }
  componentDidMount(){
    sendRequest("./cardsArr.json")
      .then (result => {
        this.setState({cards: result.cards})
      });
    
    if (localStorage.getItem("inCart") || localStorage.getItem("inFavorite")) {
        const parseInCart = JSON.parse(localStorage.getItem("inCart"));
        const parseInFavorite = JSON.parse(localStorage.getItem("inFavorite"));
        this.setState({
          ...this.state,
          inCart: parseInCart,
          inFavorite: parseInFavorite,
        });
      }  
    }

  componentDidUpdate() {
    localStorage.setItem(
      "inCart",
      JSON.stringify(this.state.inCart)
      );
    localStorage.setItem(
      "inFavorite",
      JSON.stringify(this.state.inFavorite)
      );
    }
    
 addToCart = (item) => {
    let {inCart} = this.state;
    inCart.push(item)
    this.setState({
      inCart: inCart 
    })
  };
  
  addRemoveFavorite = (item) => {
    let {inFavorite} = this.state;
    if (inFavorite.includes(item)){
      
      let newFav = inFavorite.filter(card => card.cardId !== item.cardId)
      this.setState({
        inFavorite: newFav
      })
    } else {
        inFavorite.push(item);
        this.setState({
          inFavorite: inFavorite
        })
      }
    }
  
  render(){

    return(
      <>
      <Header inCart = {this.state.inCart} inFavorite = {this.state.inFavorite} />
      {this.state.cards && <Cards 
                              cards = {this.state.cards} 
                              inCart = {this.state.inCart}
                              inFavorite = {this.state.inFavorite}
                              addToCart = {this.addToCart}
                              addRemoveFavorite = {this.addRemoveFavorite}
                            />}
      <Footer/>
      
      </>
    )
}
}
export default App;
