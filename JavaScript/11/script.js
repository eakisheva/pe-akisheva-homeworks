const eyeIcons = document.querySelectorAll('.fa-eye');
eyeIcons.forEach(icon => icon.addEventListener('click',function (){
    const input = icon.closest('.input-wrapper').querySelector('input');
    input.setAttribute('type', 'text');
}));
const inputs = document.querySelectorAll('.input-password');
inputs.forEach(input => input.addEventListener('blur',function (){
    input.setAttribute('type', 'password');
}));
document.querySelector('.btn').addEventListener('click', function(event) {
    event.preventDefault();
    checkPassword()});

function checkPassword() {
    const password1 = document.querySelector('#main-password').value;
    const password2 = document.querySelector('#confirm-password').value;
    if (password1 === password2) {
        if (document.body.contains(document.querySelector('.error'))) { document.querySelector('.error').remove();
        };
        alert('You are welcome')
    } else {
        if (document.body.contains(document.querySelector('.error'))) {
            return
        } else showError();
    }
}
function showError(){
    const errorElement = document.createElement('p');
    errorElement.className = 'error';
    errorElement.innerHTML = 'Потрібно ввести однакові значення';
    document.querySelector('#confirm-password').after(errorElement);
}