1. Document Object Model (DOM) - об'єктна модель документа, представляє HTML-документ у вигляді дерева тегів. DOM-дерево формується при завантаженні HTML-документа, теги стають вузлами-елементами, текст - текстовими вузлами.

2. innerText - це тільки текст, вміст тега. innerHTML - вміст з HTML розміткою. Наприклад, якщо ми хочемо додати розмітку на сторинку <p>Some paragraph</p> і використаємо div.innerText = '<p>Some paragraph</p>', то на сторінці побачимо саме цей рядок. Якщо ж запишемо div.innerHTML = '<p>Some paragraph</p>', то всередені div отримаємо параграф р з  текстом Some paragraph.

3. let element = document.getElementById('elem-id')
let elements = document.getElementsByTagName('div')
let elements = document.getElementsByClassName('className')
це застарілі методи, використовуються рідко

let elements = document.querySelectorAll('div')
let element = document.querySelector('#elemId')
ці методи більш універсальні

