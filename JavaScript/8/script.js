elemP = document.getElementsByTagName('p');
for (let i = 0; i < elemP.length; i++) {
    elemP[i].style.backgroundColor = '#ff0000';
};

let optionList = document.querySelector('#optionsList');
console.log(optionList);
console.log(optionList.parentElement);
// 1 variant
console.log(optionList.childNodes);
// 2 variant
for (let node of optionList.childNodes) {
    console.log(node); 
  }
let testParagraph = document.querySelector('#testParagraph');
testParagraph.innerText = 'This is a paragraph';

let mainHeaderChildren = document.querySelector('.main-header').children;
console.log(mainHeaderChildren);
for (let child of mainHeaderChildren) {
    child.classList.add ('nav-item');
    console.log(child);
}

let sectionTitle = document.querySelectorAll('.section-title');
console.log(sectionTitle.length); // елементів із класом section-title нема, але якщо б були, видаляємо так
for (let child of sectionTitle) {
    child.classList.remove ('section-title');
}