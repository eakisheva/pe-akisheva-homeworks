const buttons = document.querySelectorAll('.btn');
let attributs = {};
let attributsArr =[];
buttons.forEach(element => {  
    attributs = Object.assign(attributs, element.dataset);
    attributsArr.push(attributs.key);
})
document.body.addEventListener('keyup', (event) => {
    if (attributsArr.includes(event.code)) {
        let isActive = document.querySelector('.active');
        if (isActive) {
            isActive.classList.remove('active');
        }
        const activeBtn = document.querySelector(`.btn[data-key = "${event.code}"]`);
        activeBtn.classList.add('active');
    }
})