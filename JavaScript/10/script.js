let tabs = document.querySelectorAll('.tabs-title');
tabs.forEach(tab => {
    tab.addEventListener('click', function(){
        openArticle(tab, tab.dataset.tab);
    })
});
function openArticle (tabName, articleName) {
    tabs.forEach(tab => tab.classList.remove('active'));
    tabName.classList.add('active');
    let contentItem = document.querySelectorAll('.tabs-content-item');
    contentItem.forEach(content => {
        content.classList.add('inactive');
        if (content.dataset.content === articleName) {
            content.classList.remove('inactive')
        }
        });
}
