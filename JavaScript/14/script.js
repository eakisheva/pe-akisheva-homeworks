let optionList = document.querySelector('#optionsList');
console.log(optionList);
console.log(optionList.parentElement);
// 1 variant
console.log(optionList.childNodes);
// 2 variant
for (let node of optionList.childNodes) {
    console.log(node); 
  }
let testParagraph = document.querySelector('#testParagraph');
testParagraph.innerText = 'This is a paragraph';

let mainHeaderChildren = document.querySelector('.main-header').children;
console.log(mainHeaderChildren);
for (let child of mainHeaderChildren) {
    child.classList.add ('nav-item');
    console.log(child);
}

// 14 Homework

function changeTheme() {
    let buttons = document.querySelectorAll('.btn-primary');
    buttons.forEach((button) => {
        button.classList.toggle('green-theme');
    });
   let elemP =document.getElementsByTagName('p')
    for (let i = 0; i < elemP.length; i++) {
        elemP[i].classList.toggle('green-theme');
    };
    let headerElement = document.querySelector('.main-header');
    headerElement.classList.toggle('green-theme');
    document.querySelector('.header-title-selected').classList.toggle('green-theme');
    if (headerElement.classList.contains('green-theme')) {
        localStorage.setItem('colorTheme', 'green');
    } else {
        localStorage.clear();
    }
}
window.onload = function () {
    if(localStorage.getItem('colorTheme')) {
        changeTheme()
    } 
}
document.querySelector('.change-theme-btn').addEventListener('click', () => {
    changeTheme()});
