$('.tabs-service-title').on('click', function(){
    $('.tabs-service-title').removeClass('active-tab');
    $(this).addClass('active-tab');
    $('.tabs-service-content').addClass('hidden');
    let $tabName = ($(this).data('tab'));
    $(`.tabs-service-content[data-content = ${$tabName}]`).removeClass('hidden')
} )

$('.btn-load').on('click', function() {
    $('.loader-container').removeClass('hidden');
    setTimeout (function(){
        $('.loader-container').remove();
        $('.btn-load').remove();
        // $('.unload').show('slow');
        $('.unload').removeClass('unload')
    }, 2000);
})

$('.portfolio-menu-title').on('click', function(){
    $('.portfolio-menu-title').removeClass('active-portfolio')
    $(this).addClass('active-portfolio');
    $('.portfolio-item').addClass('hidden');
    let $portfolioItem = ($(this).data('portfolio-tab'));
    $(`.portfolio-item[data-portfolio-content = ${$portfolioItem}]`).removeClass('hidden');
    if ($portfolioItem  === "All") {
    $('.portfolio-item').removeClass('hidden');   
    };
}) 

  const swiper = new Swiper(".mySwiper", {
    loop: true,
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesProgress: true,
  });
  const swiper2 = new Swiper(".mySwiper2", {
    loop: true,
    spaceBetween: 10,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    thumbs: {
      swiper: swiper,
    },
  });